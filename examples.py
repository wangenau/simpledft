"""
Example calculations for the SimpleDFT code, including H, He, and H2.
References for the DFT++ formulation: https://doi.org/10.1016/s0010-4655(00)00072-2
Code annotations reference this master thesis: http://dx.doi.org/10.13140/RG.2.2.27958.42568/2
More documentation can be found inside the eminus routines: https://gitlab.com/wangenau/eminus
"""

import time

from simpledft import Atoms, SCF


def calculate(atoms):
    start = time.perf_counter()
    etot = SCF(atoms).run()
    print("Etot({}) = {:.6f} Eh".format(atoms.atom, etot))
    print(" {:.6f} seconds".format(time.perf_counter() - start))


if __name__ == "__main__":
    H = Atoms(["H"], [0, 0, 0], 16, 16, [1], [60, 60, 60], [1])
    calculate(H)
    # Output:  Etot(["H"]) = -0.438418 Eh

    He = Atoms(["He"], [0, 0, 0], 16, 16, [2], [60, 60, 60], [2])
    calculate(He)
    # Output:  Etot(["He"]) = -2.632035 Eh

    # Experimental geometry from CCCBDB: https://cccbdb.nist.gov/exp2x.asp?casno=1333740&charge=0
    H2 = Atoms(["H", "H"], [[0, 0, 0], [1.4, 0, 0]], 16, 16, [1, 1], [60, 60, 60], [2])
    calculate(H2)
    # Output:  Etot(["H", "H"]) = -1.113968 Eh
